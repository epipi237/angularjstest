angular.module('myApp')
.config(['$httpProvider', function ($httpProvider) {
  var self = this;

  $httpProvider.interceptors.push('messages','$q',
    function(messages,$q){
      return {
        'request': function(config) {
          if(config.data) {
          	 // console.log(config.data);

             if(config.data.code == 404){
             	      var rawtime = new Date();
		              var time = rawtime.getHours() + ':' + rawtime.getMinutes() + ':' + rawtime.getSeconds();
             		  messages.add(config.data.code,time,config.data.phrase);
		              var canceler = $q.defer();
		              config.timeout = canceler.promise;
		              canceler.resolve();
             }else{
		             var rawtime = new Date();
		             var time = rawtime.getHours() + ':' + rawtime.getMinutes() + ':' + rawtime.getSeconds();
		   		     messages.add(config.data.code,time,config.data.phrase);
		  		    
             }
          
          }
          return config;
        }
      };
    }
  );
}]);
