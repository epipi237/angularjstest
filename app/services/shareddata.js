angular.module('myApp').factory('messages', function(){
  var messages = {};

  messages.list = [];

  messages.add = function(code,time,phrase){
    messages.list.push({code: code, time: time,phrase:phrase});
    //console.log(messages.list)
  };

  return messages;
});