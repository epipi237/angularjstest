'use strict';
 
angular.module('myApp.home', ['ngRoute'])
 
// Declared route 
.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/home', {
        templateUrl: 'home/home.html',
        controller: 'HomeCtrl'
    });
}])
 
// Home controller
.controller('HomeCtrl', ['$scope','$http','messages',function($scope,$http,messages) {
var self=this;
$scope.text='';
$scope.checker=0;
 $scope.messages = messages.list;
 $scope.forminfo=[];
 
   $scope.$watch(function(scope) { return $scope.messages.length },
         function() {
         if($scope.messages.length!=0){
         	if($scope.messages[$scope.messages.length-1].code==404){
         		$scope.text="This is not good. you got a 404";
         		$scope.checker=1;
         	}else{
         		$scope.text="This is awesome. You got it";
         		$scope.checker=2;
         	}
         $('#modclick').click();
         console.log($scope.messages[$scope.messages.length-1].code)

              	}
              }
             );




 $scope.submit=function(){

 	console.log($scope.messages);
	$http.get("http://www.omdbapi.com",{
        data: {
          code: $scope.forminfo.code,
          phrase: $scope.forminfo.phrase
        }
	})
    .then(function(response){ 
    	
    })
 }



}]);