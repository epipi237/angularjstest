angular.module('myApp')
.directive('sbMaxPrecision', function(){
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, element, attributes, ngModel){
      
      function maxPrecision(value){
        if(!isNaN(value)){
          var validity = countDecimalPlaces(value) <= attributes.sbMaxPrecision;
          ngModel.$setValidity('max-precision', validity);
        }
        
        return value;
      }
      
      ngModel.$parsers.push(maxPrecision);
      ngModel.$formatters.push(maxPrecision); 
    }
    
  };
})
function countDecimalPlaces(value){
  var decimalPos = String(value).indexOf('.');
  if(decimalPos === -1){
    return 0;
  }else{
    return String(value).length - decimalPos -1;
  }
}